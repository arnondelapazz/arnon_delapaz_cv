import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import '../App.css';

export default function AppNavbar() {

    return (

        <Navbar expand="lg" className="navbarSection sticky-top py-3" >
		  <Container fluid>
		    <Navbar.Brand href="#home" id="navbarBrand">ACDP</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="ml-auto">
		        <Nav.Link href="#home" className="navbarLink">HOME</Nav.Link>
		        <Nav.Link href="#skillset" className="navbarLink">SKILLSET</Nav.Link>
		        <Nav.Link href="#qualification" className="navbarLink">QUALIFICATION</Nav.Link>
		        <Nav.Link href="#projects" className="navbarLink">PROJECTS</Nav.Link>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

    )

}