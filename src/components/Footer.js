import React from 'react';
import { Container, Col, Row} from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import '../App.css';

export default function Footer() {

	
	return (

		<Container fluid className="bg pt-5 pb-2">
		<Container>
			<Col md={12}>
			<div className="text-center pt-3">
				<a href="https://www.linkedin.com/in/arnon-christopher-de-la-paz-4b365b220/" target="_blank" className="social_link px-1"><Unicons.UilLinkedinAlt  size="40"/></a>
				<a href="https://github.com/arnondp" target="_blank" className="social_link px-1"><Unicons.UilGithub   size="40"/></a>
				<a href="https://www.facebook.com/ArnonDP" target="_blank" className="social_link px-1"><Unicons.UilFacebookF  size="40"/></a>
				<a href="https://www.instagram.com/arnondp/?hl=en" target="_blank" className="social_link px-1"><Unicons.UilInstagram  size="40"/></a>
			</div>
			</Col>
			<Col lg={12} className="pt-4 pb-1">
		        <h3 className="text-center copyR">© Copyright 2021 Arnon Christopher De La Paz</h3>          
		    </Col>
		</Container>
		</Container>	

		)


}