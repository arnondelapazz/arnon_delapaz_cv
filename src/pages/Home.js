import React from 'react';
import { Container, Col, Row} from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import '../App.css';
import myPic from '../images/my-pic.jpg';

export default function Home() {

	return (
		<>
		<Container fluid className="bg pt-5 section" id="home">
			<Container className="bg2 mt-5">
			<Row className="mt-5">
				<Col md={5}  className="px-0 my_pic">
					
				</Col>
				<Col md={7}  className="px-4 py-4">
					<h1 className="mt-3 my_name">ARNON CHRISTOPHER DE LA PAZ</h1>
					<h3 className="my_jtitle">Full Stack Developer</h3>
					<p className="about_me">
					A full stack developer that specializes in MERN STACK. I would likely ought to contribute in the firm by utilizing
					my e-commerce web application development skills. I have been produced a number of capstone projects that
					involved both front and back end development. Along with my passion for learning and freshly acquired coding
					skills, I can able to contribute to empowering your team.
					</p>
					<h4 className="details">Phone:</h4>
					<p className="my_details">+63-995-465-3972</p>
					<h4 className="details">Email:</h4>
					<p className="my_details">arnondelapazz@gmail.com</p>
					<h4 className="details">Address:</h4>
					<p className="my_details">Marikina City, NCR-Philippines</p>
					<h4 className="details">Date of birth:</h4>
					<p className="my_details">January 25, 1995</p>
				</Col>
			</Row>
			</Container>
		</Container>
		<Container fluid className="bg2 pb-5">
			<Container className="bg text-center py-4 mb-5">
				<a href="https://www.linkedin.com/in/arnon-christopher-de-la-paz-4b365b220/" target="_blank" className="social_link px-1"><Unicons.UilLinkedinAlt  size="30"/></a>
				<a href="https://github.com/arnondp" target="_blank" className="social_link px-1"><Unicons.UilGithub   size="30"/></a>
				<a href="https://www.facebook.com/ArnonDP" target="_blank" className="social_link px-1"><Unicons.UilFacebookF  size="30"/></a>
				<a href="https://www.instagram.com/arnondp/?hl=en" target="_blank" className="social_link px-1"><Unicons.UilInstagram  size="30"/></a>
			</Container>	
		</Container>
		</>
		)

}