import React, {useEffect} from 'react';
import { Container, Col, Row, Carousel, Image} from 'react-bootstrap';
import '../App.css';
import Capstone1 from '../images/Capstone-1.png';
import RestAPI from '../images/RestAPI.jpg';
import Capstone3 from '../images/capstone-3.png';
import Aos from "aos";
import "aos/dist/aos.css"


export default function Projects() {

	useEffect(() => {
		Aos.init({duration:1000});
	}, [])


	return(
		<Container fluid className="bg2 section pt-5 pb-5 carousel1" id="projects">
		<Container className=" carousel1">
		<h1 className="text-center sec-title pt-2">PROJECTS</h1>
		<p className="text-center sec-subtitle">My Latest Works</p>

		<Carousel className="carousel_proj mt-3 mb-5" data-aos="zoom-out-up">
		  <Carousel.Item interval={2500}>
		    <Image
		      fluid
		      className="d-block projects"
		      src={Capstone1}
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h3 className="p_title">Capstone 1 - Static Portfolio Website</h3>
		      <p className="p_desc">created a portfolio with a focal point of home/landing page that focusing in informing and empowering the visitor of the website. There are also a contact section where you can see my email and phone number. As you go along with the website you can see a responsive nav bar, footer, and most especially the social media icons that linked to different social media accounts.</p>
		      <p>Tools: HTML, CSS, Javascript & Github</p>
		  	<div className="text-center mt-3">
		      <a className="btn_link" href="https://arnondp.github.io/capstone-1-delapaz/" target="_blank">Check It Out!</a>
		     </div>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item interval={2500}>
		    <Image
		      fluid
		      className="d-block projects"
		      src={RestAPI}
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h3 className="p_title">Capstone 2 - E-Commerce API</h3>
		      <p className="p_desc">Develop an E-Commerce API with a rundown of user registration and authentication, admin authorization and management, non-admin user check-out, retrieve single and active products, retrieve authenticated user’s orders, non-admin user display products per order, and lastly hosted the application via Heroku.</p>
		      <p>Tools: MongoDB, ExpressJS, NodeJS, Postman & Heroku</p>
		     <div className="text-center mt-3">
		      <a className="btn_link" href="https://gitlab.com/zui1/b123/capstone2-delapaz" target="_blank">Check It Out!</a>
		      </div>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item interval={2500}>
		    <Image
		      fluid
		      className="d-block projects"
		      src={Capstone3}
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h3 className="p_title">Capstone 3 - MERN E-Commerce</h3>
		      <p className="p_desc">Generate an E-Commerce APP with the features of needes in a APP format, from log in and register, add/enable/disable a product for admin only, view active products for regular and guest users, admin dashboard, view singe product, add to cart for logged in users, added check out to create an order, and most especially hosted the application via Vercel.</p>
		      <p>Tools: MongoDB, ExpressJS, ReactJS, NodeJS & Vercel</p>
		      <div className="text-center mt-3 btn_l">
		      	<a className="btn_link" href="https://laraos-7gw6evblh-arnondelapazz.vercel.app/" target="_blank">Check It Out!</a>
		      </div>
		      
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>
		</Container>
		</Container>
		)
}