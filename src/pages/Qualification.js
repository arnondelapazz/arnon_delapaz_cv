import React, {useEffect} from 'react';
import { Container, Col, Row, Card} from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import '../App.css';
import Aos from "aos";
import "aos/dist/aos.css"


export default function Qualification() {

	useEffect(() => {
		Aos.init({duration:1000});
	}, [])


	return (

		<Container fluid className="bg2 section pt-5 pb-4" id="qualification">
			<Container>
			<h1 className="text-center sec-title mt-4">QUALIFICATION</h1>
			<p className="text-center sec-subtitle">My Journey</p>
				<Row className="pt-4">	
				<Col md={12}>
						<h3 className="quali"><Unicons.UilBag size="30"/> Experience</h3>
						<Container>
							<Row className="mb-5">
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span className="q_year"><Unicons.UilCalendarAlt/> 2021</span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Property Presenter</Card.Title>
									<Card.Text className="q_name">MEGAEAST PROPERTIES, INC</Card.Text>
							
										<p>
											 • Provide guidance and assist sellers and buyers in marketing and purchasing property for the right
											price under the best terms.
										</p>
										<p>
											 • Determine clients' needs and financials abilities to propose solutions that suit them.
										</p>
									
								</Card>
								</Col>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_year"><Unicons.UilCalendarAlt/> 2021</span>
									
									<div className="q_line2"></div>	
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Account Management Executive</Card.Title>
									<Card.Text className="q_name">We Move People and Things, Inc</Card.Text>
							
										<p>
											 • Generate sales among client accounts
										</p>
										<p>
											 • Operates as the point of contact for assigned merchants
										</p>	
								</Card>
								</Col>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_year"><Unicons.UilCalendarAlt/> 2020</span>
									
									<div className="q_line2"></div>	
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Store Manager</Card.Title>
									<Card.Text className="q_name">GOLDEN ABC, Inc.
									</Card.Text>
							
										<p>
											• Managed and oversees all facets of the daily operations, ensuring compliance with company policies and regulation
										</p>
										<p>
											 • Oversees the supervision of personnel, which includes work allocation, training and problem resolution and evaluates on the job performance.
										</p>
										<p>
											• Ensures the execution of the visual presentation direction and updates.
										</p>	
								</Card>
								</Col>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span className="q_year"><Unicons.UilCalendarAlt/> 2019</span>
									
									<div className="q_line2"></div>	
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Department Supervisor</Card.Title>
									<Card.Text className="q_name">Metro Retail Stores Group, Inc.
									</Card.Text>
							
										<p>
											 • Managed all aspects of Department and ensured customer satisfaction.
										</p>
										<p>
											 • Maintain inventory and ensure items are in stock.
										</p>	
										<p>
											• Organize and distribute staff schedules.
										</p>
								</Card>
								</Col>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_year"><Unicons.UilCalendarAlt/> 2018</span>
									
									<div className="q_line2"></div>
									<span className="q_year pl-5"><Unicons.UilRocket size="30"/></span>	
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Store Supervisor</Card.Title>
									<Card.Text className="q_name">Suyen Corporation</Card.Text>
							
										<p>
											 • Managed all aspects of store presentation and ensured customer satisfaction
										</p>
										<p>
											• Manage retail staff, including cashiers and people working on the floor
										</p>
										<p>
											• Work on store displays.

										</p>	
								</Card>
								</Col>
							</Row>
						</Container>
					</Col>
					<Col md={12} className="pt-5">
						<h3 className="quali"><Unicons.UilGraduationCap  size="30"/> Education</h3>
						<Container>
							<Row>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_year"><Unicons.UilCalendarAlt/> 2021</span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Zuitt Coding Bootcamp</Card.Title>
									<p className="q_name">• Developer Career Program</p>
								</Card>
								</Col>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_year"><Unicons.UilCalendarAlt/> 2021</span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Udemy</Card.Title>
									<p className="q_name">• Build your own awesome responsive Personal PORTFOLIO site</p>
									<p className="q_name">• The Complete 2021 Web Development Bootcamp</p>
								</Card>
								</Col>
								<Col sm={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_year"><Unicons.UilCalendarAlt/> 2017</span>
									
									<div className="q_line3"></div>
									<span className="q_year pl-5"><Unicons.UilRocket size="30"/></span>
									
								</Col>
								<Col sm={9}>
								<Card className="educ" data-aos="slide-left">
									<Card.Title className="q_title">Our Lady of Fatima University</Card.Title>
									<p className="q_name">• Bachelor of Science in Business Administration Major in Business Management</p>
									
								</Card>
								</Col>

							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Container>

		)
}