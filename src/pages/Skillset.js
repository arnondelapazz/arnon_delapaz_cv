import React, {useEffect} from 'react';
import { Container, Col, Row, Card, ProgressBar } from 'react-bootstrap';
import '../App.css';
import html from '../images/html.png';
import css from '../images/css.png';
import javascript from '../images/javascript.png';
import bootstrap from '../images/bootstrap.png';
import mongodb from '../images/mongodb.png';
import express from '../images/express.png';
import react from '../images/react.png';
import nodejs from '../images/node-js.png';
import Aos from "aos";
import "aos/dist/aos.css";

export default function Skillset() {

	useEffect(() => {
		Aos.init({duration:1400});
	}, [])


	return (

		<Container fluid className="bg2 section pt-5 pb-4" id="skillset">
			<Container className="mb-5">
				<h1 className="text-center sec-title mt-4">SKILLSET</h1>
				<p className="text-center sec-subtitle">My Skillsets</p>
				<Row className="pt-4">
					<Col xs={6} md={3} data-aos="fade-right">
					<Card className="card-skill">
						<Card.Img src={html} alt="html"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">HTML</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-down">
					<Card>
						<Card.Img  src={css} alt="css"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">CSS</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-down">
					<Card>
						<Card.Img  src={javascript} alt="javascript"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">Javascript</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-left">
					<Card>
						<Card.Img  src={bootstrap} alt="bootstrap"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">Bootstrap</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-right">
					<Card>
						<Card.Img  src={mongodb} alt="mongodb"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">MongoDB</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-up">
					<Card>
						<Card.Img  src={express} alt="express"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">ExpressJS</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-up">
					<Card>
						<Card.Img  src={react} alt="react"/>
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">ReactJS</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
					<Col xs={6} md={3} data-aos="fade-left">
					<Card>
						<Card.Img src={nodejs} alt="nodejs"/>	
						<Card.ImgOverlay>
						    <Card.Title className="skill-name">NodeJS</Card.Title>
						</Card.ImgOverlay>
					</Card>	
					</Col>
				</Row>
			</Container>
		</Container>

		)

}