import React from 'react';
import './App.css';
import { Container } from 'react-bootstrap';


import Appnavbar from './components/Appnavbar';
import Home from './pages/Home';
import Skillset from './pages/Skillset';
import Qualification from './pages/Qualification';
import Projects from './pages/Projects';
import Footer from './components/Footer';


export default function App() {
  return (
    <>

          <Appnavbar/>
          <Home />   
          <Skillset />   
          <Qualification />   
          <Projects />   
          <Footer />   
  

    </>
  )
}


